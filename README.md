tmsim - Python-based Time Machine Simulator

Just a dumb project I thought of as a good excuse to practice my Python. It's
based on an old game idea I had a long time ago when I was first learning how
to program. It's complex enough and something easy enough for me to
conceptualize, so it makes for the perfect project to learn on.

