import os
import sys
import random
from math input floor

"""
tm.py - Time Machine Class

Creates the time machine object which the simulator interacts with throughout
the game.

Difficulty Scale:
    wear_risk   tear_risk
--------------------------------------------------------------------------
0 -

"""

# Generic class for all objects inside the TM
class Component(object):

    # Attributes
    component = "component"
    is_replaceable = False
    is_breakable = False

    def __init__(self):
        # Set default wear and tear of components
        self.wear = 0
        self.tear = 0
        self.rebuilt = False
        self.is_broken = False

    def wear_and_tear(self, wear_risk, tear_risk):
        # Calculate and handle wear and tear based on the amounts given,
        # but don't go above 100
        self.wear += min(random.randint(0, wear_risk), 100)
        self.tear += min(random.randint(0, tear_risk), 100)

    def field_repair(self, amount, wear_risk):
        # Do a field repair without a repair kit, but at a wear/damage risk.
        self.tear -= max(random.randint(0, amount), 0)
        self.wear += min(random.randint(0, wear_risk), 100)

    def repair(self, amount):
        # Repair the component's damage based on the amount givenself,
        # but don't go below 0. Uses a repair kit if not in the workshop.
        self.tear -= max(random.randint(0, amount), 0)

        # TODO: For expert mode, add in variable for times since rebuild and
        #       limit it to whatever number (3, 5, etc.) through a variable.

    def overhaul(self, amount):
        # Perform an overhaul, but only if an overhaul kit is available. can
        # overhaul a part

    def rebuild(self, amount):
        # Rebuild component for a larger amount, but only allow a rebuild
        # once before time travelling, and only if you have a rebuild kit.
        # Returns 0 for a successful rebuild, 1 if it has already been done.
        # Returns 2 if there is no rebuild kit.

        # Check for previous rebuild
        if self.rebuilt:
            return 1    # Error code for rebuilding once per workshop visit

        if self.rebuild_kits == 0:
            return 2    # Error code for rebuild kit missing

        # Rebuild the part's wear and tear by the amount given, with a
        # minimum of half of the amount given (rounded down).
        self.wear -= max(random.randint(floor(amount / 2), amount))
        self.tear -= max(random.randint(floor(amount / 2), amount))

        # Set rebuilt flag to true
        self.rebuilt = True

    def used(self):
        # Routine to follow whenever the part is used.

        # TODO: Build this out more to allow wear and tear to be called.
        # This can be the routine where the breakage detection happens.

        self.rebuilt = False


# Component class for PC Parts that are replaceable
class PCPart(Component):

    # Attributes
    component = "pcpart"
    replaceable = True

    def __init__(self):
        # Set number of spare parts to zero
        spares = 0

    def

# Component class for time machine parts that are breakable
class TMPart(Component):

    # Attributes
    component = "tmpart"
    breakable = True

    def __init__(self):
        # Start off working
        is_broken = False

    #TODO: Need to do the whole break fix thing

# Class for the time machine computer system
class Computer(object):
    pass


# Class for the time machine cockpit
class Cockpit(object):
    pass


# Class


# Class for the time machine

# Main Time Machine Class
class TimeMachine(object):

    def __init__(self, difficulty):
        self.difficulty = difficulty

        self.mobo = PCPart()
        self.cpu = PCPart()

    def explore(self):
        pass


def testing():
    # Class testing goes here
    my_tm = TimeMachine()


# If run directly, test the class
if __name__ == "__main__":
    random.seed()
    testing()
